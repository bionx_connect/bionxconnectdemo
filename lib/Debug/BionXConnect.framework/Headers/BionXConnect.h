//
//  BionXConnect.h
//  BionXConnect
//
//  Created by Yannick Laberge on 2014-09-12.
//  Copyright (c) 2014 Y Code Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BionXConnect.
FOUNDATION_EXPORT double BionXConnectVersionNumber;

//! Project version string for BionXConnect.
FOUNDATION_EXPORT const unsigned char BionXConnectVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BionXConnect/PublicHeader.h>


