import UIKit
import Foundation
import BionXConnect

class BikeOptionsViewController: UIViewController, BikeBridgeDelegate {
    @IBAction func onTurnBikeOffRequest(sender: AnyObject) {
        let parent = parentViewController as! BikeConnectController
        parent.bikeDescriptor.bikeBridge?.requestShutdown()
    }

}
