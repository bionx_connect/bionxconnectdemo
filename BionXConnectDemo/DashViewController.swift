//
//  DashViewController.swift
//  ridebionx
//
//  Created by Yannick Laberge on 2014-08-15.
//  Copyright (c) 2014 Y Code Lab. All rights reserved.
//

import UIKit
import Foundation
import BionXConnect

class DashViewController: UIViewController, BikeBridgeDelegate {
    @IBOutlet weak var speedLabel: UILabel!

     func didUpdateBridgeValues(bridge: BikeBridge, values: [String : AnyObject]) {
        if let speed = values["speed"] as? Float {
            if let circumference = values["circumference"] as? Float {
                speedLabel.text = NSNumberFormatter().stringFromNumber(speed * circumference * 60.0 / 1000.0)
            }
        }
    }
    @IBAction func onLightsValueChange(sender: AnyObject) {
        let parent = parentViewController as! BikeConnectController
        parent.bikeDescriptor.bikeBridge?.requestLights((sender as! UISwitch).on)
    }
}
