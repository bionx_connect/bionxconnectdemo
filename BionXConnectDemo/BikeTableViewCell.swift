//
//  BikeTableViewCell.swift
//  ridebionx
//
//  Created by Yannick Laberge on 2014-08-05.
//  Copyright (c) 2014 Y Code Lab. All rights reserved.
//

import UIKit

class BikeTableViewCell: UITableViewCell {
    @IBOutlet weak var bikeImage: AsyncImageView!
    @IBOutlet weak var bikeOemName: UILabel!
    @IBOutlet weak var bikeModelName: UILabel!
    @IBOutlet weak var bikeModelYear: UILabel!
    @IBOutlet weak var bikeBatteryLevelImage: UIImageView!
    @IBOutlet weak var bikeRSSILevelLabel: UILabel!

    var bikeDescriptor: BikeDescriptor? {
        didSet {
            let bridge = bikeDescriptor!.bikeBridge!
            bikeModelName.text = bridge.name
            bikeImage.imageURL = NSURL(string: "http://ridebionx.com/wp-content/uploads/2014/03/thb-d-series.jpg")
            bikeBatteryLevelImage.image = bridge.isInRange ? UIImage(named: "BatteryLevelFull") : nil
            bikeRSSILevelLabel.text = bridge.isInRange ? "\(bikeDescriptor!.bikeBridge!.RSSI) dB" : ""
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
