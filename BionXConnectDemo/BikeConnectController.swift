//
//  BikeConnectController.swift
//  ridebionx
//
//  Created by Yannick Laberge on 2014-08-06.
//  Copyright (c) 2014 Y Code Lab. All rights reserved.
//

import UIKit
import QuartzCore
import BionXConnect

class BikeConnectController: UITabBarController, UITabBarControllerDelegate, BikeBridgeDelegate {
    var bikeDescriptor: BikeDescriptor!
    var bikeBridgeManager: BikeBridgeManager!
    var connectionSegue: UIStoryboardSegue?
    var shallCancelConnection = false

    override func viewDidAppear(animated: Bool) {
        if !shallCancelConnection && !bikeDescriptor.bikeBridge!.isConnected {
            startConnection()
        }
        else if shallCancelConnection {
            navigationController?.popViewControllerAnimated(true)
        }
    }

    func startConnection() {
        if !shallCancelConnection {
            performSegueWithIdentifier("connecting", sender: self)
            bikeBridgeManager.connectBikeBridge(bikeDescriptor.bikeBridge)
        }
    }

    override func viewWillDisappear(animated: Bool) {
        let selfInNavigationController = navigationController!.viewControllers.reduce(false) { $0 || $1 as! UIViewController === self }
        if !selfInNavigationController {
            terminateBikeConnection()
        }
        super.viewWillDisappear(animated)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "connecting" {
            connectionSegue = segue
        }
    }
    
    @IBAction
    func cancelBikeConnection(seque: UIStoryboardSegue) {
        terminateBikeConnection()
    }

    func terminateBikeConnection() {
        shallCancelConnection = true
        bikeBridgeManager.cancelBikeBridgeConnection(bikeDescriptor.bikeBridge!)
    }

    func bridgeManager(manager: BikeBridgeManager!, didConnectBridge bridge: BikeBridge!) {
        dismissViewControllerAnimated(true) {}
        bikeDescriptor.bikeBridge!.delegate = self
    }

    func bridgeManager(manager: BikeBridgeManager!, didDisconnectBridge bridge: BikeBridge!) {
        if !shallCancelConnection {
            startConnection()
        }
    }

    func bridgeManager(manager: BikeBridgeManager!, didFailToConnectBridge bridge: BikeBridge!) {
    }

    func didUpdateBridgeValues(bridge: BikeBridge, values: [String: AnyObject]) {
        for controller in viewControllers! {
            if let controller = controller as? BikeBridgeDelegate {
                controller.didUpdateBridgeValues?(bridge, values: values)
            }
        }
    }

}
