//  This is the controller that manages bike discovery. When a bike appears, it displays it in a table view and when the
//  bike is not lost, it is removed (displaced in a "known bikes" list) from the table.
//
//  Copyright (c) 2014 BionX International. All rights reserved.
//

import UIKit
import CoreBluetooth
import BionXConnect


class BikeDescriptor {
    var identifier: NSUUID
    var bikeBridge: BikeBridge?

    init(identifier: NSUUID) {
        self.identifier = identifier
    }
}


class BikeTableViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource, BikeBridgeManagerDelegate {
    @IBOutlet var bikeTableView: UITableView!
    var bikeBridgeManager: BikeBridgeManager!
    var inRangeBikes: [BikeDescriptor] = []
    var knownBikes: [BikeDescriptor] = []
    var bikeConnectController: BikeConnectController?

    override func viewDidLoad() {
        super.viewDidLoad()

        bikeBridgeManager = makeBikeBridgeManager()
        bikeBridgeManager.delegate = self
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2  // One for live bikes and one for known bikes (not live but that we already seen in the past).
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? inRangeBikes.count : knownBikes.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let titles = ["Bikes within range", "Known bikes"]
        return titles[section]
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("bike") as! BikeTableViewCell
        let descriptor = (indexPath.section == 0 ? inRangeBikes : knownBikes)[indexPath.row]
        cell.bikeDescriptor = descriptor
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        bikeConnectController = (segue.destinationViewController as! BikeConnectController)
        let cell = sender as! BikeTableViewCell
        bikeConnectController!.bikeBridgeManager = bikeBridgeManager
        bikeConnectController!.bikeDescriptor = cell.bikeDescriptor
    }
    
    func bridgeManager(manager: BikeBridgeManager!, didDiscoverBridge bridge: BikeBridge!) {
        let identifier = bridge.identifier

        var descriptorIndex = find(knownBikes.map{ $0.identifier }, identifier)
        var descriptor: BikeDescriptor
        if let descriptorIndex = descriptorIndex {
            descriptor = knownBikes[descriptorIndex]
            knownBikes.removeAtIndex(descriptorIndex)
            bikeTableView.deleteRowsAtIndexPaths([NSIndexPath(forItem: descriptorIndex, inSection: 1)],
                                                 withRowAnimation: UITableViewRowAnimation.Automatic)
        }
        else {
            descriptor = BikeDescriptor(identifier: identifier)
        }
        descriptor.bikeBridge = bridge

        inRangeBikes.append(descriptor)
        bikeTableView.insertRowsAtIndexPaths([NSIndexPath(forItem: inRangeBikes.count - 1, inSection: 0)],
                                             withRowAnimation: UITableViewRowAnimation.Automatic)
    }

    func bridgeManager(manager: BikeBridgeManager!, didUpdateBridgeStatus bridge: BikeBridge!) {
        if let descriptorIndex = find(inRangeBikes.map{ $0.identifier }, bridge.identifier) {
            bikeTableView.reloadRowsAtIndexPaths([NSIndexPath(forItem: descriptorIndex, inSection: 0)],
                                                 withRowAnimation: UITableViewRowAnimation.None)
        }
    }

    func bridgeManager(manager: BikeBridgeManager!, didLostBridge bridge: BikeBridge!) {
        if let descriptorIndex = find(inRangeBikes.map{ $0.identifier }, bridge.identifier) {
            let descriptor = inRangeBikes[descriptorIndex]
            inRangeBikes.removeAtIndex(descriptorIndex)
            bikeTableView.deleteRowsAtIndexPaths([NSIndexPath(forItem: descriptorIndex, inSection: 0)],
                                                 withRowAnimation: UITableViewRowAnimation.Automatic)
            knownBikes.insert(descriptor, atIndex: 0)
            bikeTableView.insertRowsAtIndexPaths([NSIndexPath(forItem: 0, inSection: 1)],
                                                 withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }

    // Simply forward bike bridge connection related messages.
    
    func bridgeManager(manager: BikeBridgeManager!, didConnectBridge bridge: BikeBridge!) {
        bikeConnectController?.bridgeManager(manager, didConnectBridge: bridge)
    }

    func bridgeManager(manager: BikeBridgeManager!, didDisconnectBridge bridge: BikeBridge!) {
        bikeConnectController?.bridgeManager(manager, didDisconnectBridge: bridge)
    }

    func bridgeManager(manager: BikeBridgeManager!, didFailToConnectBridge bridge: BikeBridge!) {
        bikeConnectController?.bridgeManager(manager, didFailToConnectBridge: bridge)
    }
}

