# BionXConnectDemo #

### What is this repository for? ###

This is a simple app that demonstrates how to use the BionXConnect framework.

### How do I get set up? ###

* Use Xcode 6.2 or more recent
* Configure the project to use your copy of BionXConnect framework (contact BionX to get it).
